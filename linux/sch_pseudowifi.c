/**
 * sch_pseudowifi.c Simple FIFO queue with logic to mimic WiFi interface.
 *
 * qdisc is modified from pfifo.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version
 * 2 of the License, or (at your option) any later version.
 *
 * @author KK Yap, <yapkke@alumni.stanford.edu>
 * @author Alexey Kuznetsov (for pfifo), <kuznet@ms2.inr.ac.ru>
 */
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/skbuff.h>
#include <net/pkt_sched.h>

MODULE_AUTHOR("ykk");
MODULE_LICENSE("GPL");

static int pseudowifi_enqueue(struct sk_buff *skb, struct Qdisc *sch)
{
  int i;

  if (likely(skb_queue_len(&sch->q) < sch->limit)) {
    struct ethhdr* ehdr;
    skb_reset_mac_header(skb);
    ehdr = eth_hdr(skb);

    for (i = 0; i < ETH_ALEN; i++) 
      if (ehdr->h_source[i] != skb->dev->perm_addr[i]) {
	//Packet source address is different from device address
	qdisc_drop(skb, sch);
	return NET_XMIT_SUCCESS;
      }

    return qdisc_enqueue_tail(skb, sch);
  }

  return qdisc_reshape_fail(skb, sch);
}


static int pseudowifi_init(struct Qdisc *sch, struct nlattr *opt)
{
  if (opt == NULL) {
    u32 limit = qdisc_dev(sch)->tx_queue_len ? : 1;
    sch->limit = limit;
  } else {
    struct tc_fifo_qopt *ctl = nla_data(opt);
    
    if (nla_len(opt) < sizeof(*ctl))
      return -EINVAL;
    
    sch->limit = ctl->limit;
  }
  
  //No bypass
  sch->flags &= ~TCQ_F_CAN_BYPASS;

  return 0;
}

static int pseudowifi_dump(struct Qdisc *sch, struct sk_buff *skb)
{
  struct tc_fifo_qopt opt = { .limit = sch->limit };
  
  if (nla_put(skb, TCA_OPTIONS, sizeof(opt), &opt))
    goto nla_put_failure;
  return skb->len;
  
 nla_put_failure:
  return -1;
}

struct Qdisc_ops pseudowifi_qdisc_ops __read_mostly = {
  .id             =       "pseudowifi",
  .priv_size      =       0,
  .enqueue        =       pseudowifi_enqueue,
  .dequeue        =       qdisc_dequeue_head,
  .peek           =       qdisc_peek_head,
  .drop           =       qdisc_queue_drop,
  .init           =       pseudowifi_init,
  .reset          =       qdisc_reset_queue,
  .change         =       pseudowifi_init,
  .dump           =       pseudowifi_dump,
  .owner          =       THIS_MODULE,
};
EXPORT_SYMBOL(pseudowifi_qdisc_ops);

static int __init pseudowifi_mod_init(void)
{
  register_qdisc(&pseudowifi_qdisc_ops);

  printk("pseudowifi module added.\n");
  return 0;
}

static void __exit pseudowifi_mod_cleanup(void)
{
  unregister_qdisc(&pseudowifi_qdisc_ops);

  printk("pseudowifi module removed.\n");
}

module_init(pseudowifi_mod_init);
module_exit(pseudowifi_mod_cleanup);
